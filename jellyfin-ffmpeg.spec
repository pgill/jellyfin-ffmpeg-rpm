%global debug_package %{nil}
%global jf_release 8
%global pkg_config_flags --static
%global _cuda_version 12.5
%global _cuda_version_rpm %(echo %{_cuda_version} | sed -e 's/\\./-/')
%global cuda_cflags $(pkg-config %{pkg_config_flags} --cflags cuda-%{_cuda_version})
%global cuda_ldflags $(pkg-config %{pkg_config_flags} --libs cuda-%{_cuda_version}) -L$(pkg-config --variable=libdir cuda-%{_cuda_version})/stubs
%global libnpp_cflags $(pkg-config %{pkg_config_flags} --cflags nppi-%{_cuda_version} nppc-%{_cuda_version})
%global libnpp_ldlags $(pkg-config %{pkg_config_flags} --libs-only-L nppi-%{_cuda_version} nppc-%{_cuda_version})

Name:           jellyfin-ffmpeg
Version:        6.0.1
Release:        %{jf_release}%{?dist}
Summary:        FFmpeg for Jellyfin
License:        GPL3
URL:            https://github.com/jellyfin/jellyfin-ffmpeg

ExclusiveArch:  x86_64


BuildRequires:  AMF-devel
BuildRequires:  clang
BuildRequires:  cuda-driver-devel-%{_cuda_version_rpm}
BuildRequires:  cuda-minimal-build-%{_cuda_version_rpm}
BuildRequires:  libnpp-devel-%{_cuda_version_rpm}
BuildRequires:  fdk-aac-devel
BuildRequires:  gmp-devel
BuildRequires:  glslang-devel
BuildRequires:  lame-devel
BuildRequires:  libass-devel
BuildRequires:  libbluray-devel
BuildRequires:  libchromaprint-devel
BuildRequires:  libdav1d-devel
BuildRequires:  libdrm-devel
BuildRequires:  libopenmpt-devel
BuildRequires:  libplacebo-devel
BuildRequires:  libshaderc-devel
BuildRequires:  libtheora-devel
BuildRequires:  libunistring-devel
BuildRequires:  libva-devel
BuildRequires:  libvpl-devel
BuildRequires:  libvpx-devel
BuildRequires:  libwebp-devel
BuildRequires:  nasm
BuildRequires:  numactl-devel
BuildRequires:  ocl-icd-devel
BuildRequires:  opencl-headers
BuildRequires:  openssl-devel
BuildRequires:  opus-devel
BuildRequires:  nv-codec-headers
BuildRequires:  srt-devel
BuildRequires:  svt-av1-devel
BuildRequires:  x264-devel
BuildRequires:  x265-devel
BuildRequires:  zimg-devel
BuildRequires:  zvbi-devel


Source0: https://github.com/jellyfin/jellyfin-ffmpeg/archive/refs/tags/v%{version}-%{jf_release}.tar.gz


%description
%{summary}.


%prep
%autosetup -n %{name}-%{version}-%{jf_release}
for __PATCH in $(find ./debian/patches -name '*.patch' | sort); do
  patch -p1 < "$__PATCH"
done


%build
./configure \
  --prefix=%{_prefix} \
  --bindir=%{_bindir} \
  --datadir=%{_datadir}/%{name} \
  --docdir=%{_docdir}/%{name} \
  --incdir=%{_includedir}/%{name} \
  --libdir=%{_libdir} \
  --shlibdir=%{_libdir} \
  --mandir=%{_mandir} \
  --arch=%{_target_cpu} \
  --cpu=%{_target_cpu} \
  --enable-runtime-cpudetect \
  --optflags="%{optflags}" \
  --extra-cflags='%{?cuda_cflags} %{?libnpp_cflags} -I%{_includedir}/rav1e' \
  --extra-ldflags='%{?__global_ldflags} %{?cuda_ldflags} %{?libnpp_ldlags}' \
  --extra-ldexeflags=-pie \
  --extra-libs='-ldl -lSPIRV-Tools -lSPIRV-Tools-opt' \
  --disable-shared \
  --enable-static \
  --pkg-config-flags='%{pkg_config_flags}' \
  --enable-gpl \
  --enable-version3 \
  --enable-nonfree \
  --enable-lto \
  --disable-ptx-compression \
  --disable-sdl2 \
  --disable-libxcb \
  --disable-xlib \
  --enable-iconv \
  --enable-zlib \
  --enable-libfreetype \
  --enable-libfribidi \
  --enable-gmp \
  --enable-libxml2 \
  --enable-openssl \
  --enable-lzma \
  --enable-fontconfig \
  --enable-libvorbis \
  --enable-opencl \
  --enable-amf \
  --enable-chromaprint \
  --enable-libdav1d \
  --enable-libfdk-aac \
  --enable-ffnvcodec \
  --enable-cuda \
  --enable-cuda-llvm \
  --enable-cuvid \
  --enable-nvdec \
  --enable-nvenc \
  --enable-libass \
  --enable-libbluray \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libtheora \
  --enable-libvpl \
  --enable-libvpx \
  --enable-libwebp \
  --enable-libopenmpt \
  --disable-rkmpp \
  --disable-rkrga \
  --enable-libsrt \
  --enable-libsvtav1 \
  --enable-libdrm \
  --enable-vaapi \
  --enable-vulkan \
  --enable-libshaderc \
  --enable-libplacebo \
  --enable-libx264 \
  --enable-libx265 \
  --enable-libzimg \
  --enable-libzvbi

%make_build V=1
make alltools V=1

%install
%make_install V=1
rm -r %{buildroot}%{_datadir}/%{name}/examples


%changelog
%autochangelog
